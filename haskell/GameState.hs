{-# LANGUAGE CPP #-}
module GameState where
import Model.GameInit
import Model.CarPosition
import Model.Direction
import Control.Monad.State
#if gui
import Graphics.Vty.Widgets.All
#else
#endif

import Control.Monad.State
import Data.Ord
import Data.List


nextPieces s = 
 take (numPieces s)
 . drop (currPieceIndex s + 1)
 . cycle
 . trackPieces $ s

canSwitchLeft s =
  not (null (lanesToTheLeft s)) && not (isSwitching s) && gameStarted s
canSwitchRight s =
  not (null (lanesToTheRight s)) && not (isSwitching s) && gameStarted s

safeMinBy _ [] = Nothing
safeMinBy f xs = Just (minimumBy f xs)

laneToLeft s = safeMinBy (comparing (abs . distanceFromCenter)) (lanesToTheLeft s)
laneToRight s = safeMinBy (comparing (abs . distanceFromCenter)) (lanesToTheRight s)

lanesToTheLeft s =
  let curr = currLane s in
  filter (\x -> distanceFromCenter x < distanceFromCenter curr) (trackLanes s)
lanesToTheRight s =
  let curr = currLane s in
  filter (\x -> distanceFromCenter x > distanceFromCenter curr) (trackLanes s)
next4Pieces s = take 4 $ drop (currPieceIndex s + 1) (cycle (trackPieces s))
nextPiece s = cycle (trackPieces s) !! (currPieceIndex s + 1)
currLane s = trackLanes s !! endLaneIndex (inLane s)
currPiece s = trackPieces s !! currPieceIndex s
piecesTraveled s =
  numPieces s * currLap s + currPieceIndex s

isSwitching s = 
  let carlane = inLane s in
  startLaneIndex carlane /= endLaneIndex carlane

data GameState = GameState{
  maxLapAngle :: Float,
  maxForce :: Float, -- ^ Estimated max centrifugal force on the track
  trackPieces :: [Piece], -- ^ All the pieces in the track
  trackLanes :: [Lane], -- ^ The list of all lanes.
  currPieceIndex :: Int, -- ^ the index of the piece currently on
  currLap :: Int, -- ^ Number of pieces moved
  distInto :: Float, -- ^ distance into the current piece.
  speed :: Float, -- ^ In  distance per tick.
  ourAngle :: Float, -- ^ The angle of the car.
  numLaps :: Int, -- ^ Number of laps in race
  inLane :: CarLane, -- ^ The lane currently in.
  numPieces :: Int, -- ^ Number of pieces to the track
  goingToSwitch :: Maybe Direction, -- ^ The lane we are switching to if any.
  ourName :: String, -- ^ Our name!,
  currentThrottle :: Float, -- ^ Our current throttle
  lastRecieved :: String, -- ^ Last recieved message from server
  turboAvailable :: Bool,
#if gui
  gameStarted :: Bool,
  ui :: View
#else
  gameStarted :: Bool
#endif
}


approxDistTraveled s = sum $ map (pieceLength (Lane 0 0)) (take (piecesTraveled s) (trackPieces s))
 
#if gui
data View = View{
    uiSpeed :: Widget FormattedText,
    uiCurrentThrottle :: Widget FormattedText,
    uiOurAngle :: Widget FormattedText,
    uiInLane :: Widget FormattedText,
    uiNumLaps :: Widget FormattedText,
    uiGoingToSwitch :: Widget FormattedText,
    uiTraveled :: Widget FormattedText,
    uiLastRecieved :: Widget FormattedText
}
#else
#endif
type Bot = StateT GameState IO

runBot b v = runStateT b (initGameState v)

#if gui
initGameState v = GameState 0 0.44 [] [] 0 0 0 0 0 1 (CarLane 0 0) 0 Nothing "" 0 "" False False v
#else
initGameState v = GameState 0  0.44 [] [] 0 0 0 0 0 1 (CarLane 0 0) 0 Nothing "" 0 "" False False
#endif

maybeIncreaseForce :: Float -> Bot ()
maybeIncreaseForce a
  | a < 10 = increaseMaxForceMuch
  | a < 40 = increaseMaxForceMedium
  | a < 50 = increaseMaxForceLittle
  | a < 55 = increaseMaxForceVeryLittle
  | otherwise = return () 

decreaseMaxForceMuch, increaseMaxForceMedium, increaseMaxForceMuch,  increaseMaxForceLittle :: Bot ()
decreaseMaxForceMuch   = modify (\s -> s{maxForce = 0.75 * maxForce s}) 
increaseMaxForceMuch   = modify (\s -> s{maxForce = 1.20 * maxForce s}) 
increaseMaxForceMedium = modify (\s -> s{maxForce = 1.03 * maxForce s}) 
increaseMaxForceLittle = modify (\s -> s{maxForce = 1.005 * maxForce s})
increaseMaxForceVeryLittle = modify (\s -> s{maxForce = 1.001 * maxForce s})
resetMaxLapAngle :: Bot ()
resetMaxLapAngle = modify (\s -> s{maxLapAngle = 0})

backToZero :: Bot ()
backToZero =
  modify (\s ->
   s{
    maxLapAngle = 0,
    currLap = 0,
    speed = 0,
    ourAngle = 0,
    currPieceIndex = 0,
    distInto = 0,
    goingToSwitch = Nothing,
    inLane = CarLane 0 0,
    currentThrottle = 0,
    turboAvailable = False})

updateWithRace
  Race{
    track = t,
    raceSession = r
  } = do
  updateWithSess r
  updateWithTrack t

updateWithTrack
  Track{
    pieces = p,
    lanes = l
  } =
  modify (\s -> s{
    trackPieces = p,
    trackLanes = l,
    numPieces = length p})

updateWithSess
  RaceSession {
    laps = ml
  } =
  case ml of 
    Nothing -> return ()
    (Just l) -> modify (\s -> s{numLaps = l})

updateBotName n = 
  modify (\s -> s{ourName = n})

updateWithPosition
  CarPosition{ 
   Model.CarPosition.angle = ca,
   piecePosition = PiecePosition {
    pieceIndex = pi,
    inPieceDistance = ipd,
    lane = lne,
    lap = lp
  }} = do
  oldpi <- gets currPieceIndex
  oldp <- gets currPiece
  oldDist <- gets distInto
  allLanes <- gets trackLanes
  let oldTraveled = if pi == oldpi then 0 else pieceLength (allLanes !! startLaneIndex lne) oldp - oldDist
  let newTraveled = if pi == oldpi then ipd - oldDist else ipd
  logMsg ("Speed " ++ show (oldTraveled + newTraveled) ++ " piece: " ++  show oldp ++ " Angle: " ++ show ca)
  let switching = startLaneIndex lne /= endLaneIndex lne
  when switching
    (modify (\s -> s{goingToSwitch = Nothing}))
  modify (\s -> s{
      maxLapAngle = max ca (maxLapAngle s),
      ourAngle = ca,
      currPieceIndex = pi,
      distInto = ipd,
      inLane = lne,
      currLap = lp,
      speed = oldTraveled + newTraveled
    })

logMsg str = liftIO $ appendFile "gamelog.txt" (str ++ "\n")
   
