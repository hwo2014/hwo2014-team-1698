{-#LANGUAGE OverloadedStrings, CPP #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle,hIsEOF)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Control.Monad.Trans(liftIO)
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Control.Monad.State

import Model.GameInit hiding (angle)
import Model.CarPosition
import GameState
import Decision.MakeDecision
import Model.Messages

#if gui
import UI.Vty
#else
#endif

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server,port,botname,botkey] -> do
      run server port botname botkey (JoinMessage botname botkey)
    [server,port,botname,botkey,"CREATERACE",track] -> do
      run server port botname botkey (CreateRaceMessage (JoinMessage botname botkey) track "" 1)
    [server,port,botname,botkey,"CREATERACE",track,numcars,password] -> do
      run server port botname botkey (CreateRaceMessage (JoinMessage botname botkey) track password (read numcars))
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> [CREATERACE <track> [<numcars> <password]]"
      exitFailure

run server port botname botkey message = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h . show $ message
#if gui
  view <- setupUI 
  forkDraw view
  runBot (handleMessages h) view
#else
  runBot (handleMessages h) ()
#endif

handleMessages h = do
  isEnd <- liftIO $ hIsEOF h
  if isEnd 
    then liftIO $ putStrLn "Stream Ended!"
    else do
    msg <- liftIO $ hGetLine h
    modify(\s -> s{lastRecieved = msg})
#if debug
    liftIO $ putStr "Recieved: "
    liftIO $ putStrLn msg
#endif
    return ()
    case decode (L.pack msg) of
      Just json -> do
        case fromJSON json of
          Success serverMessage -> handleServerMessage h serverMessage
          Error s -> fail $ "Error decoding message: " ++ s
      Nothing -> do
        fail $ "Error parsing JSON: " ++ (show msg)


handleServerMessage :: Handle -> ServerMessage -> Bot ()
handleServerMessage h serverMessage = do
  response <- respond serverMessage
  case response of
   -- Stop -> return ()
    (SwitchLane d) -> do
      liftIO $  do
#if debug
        putStr "sending: "
        print response
#endif
        return ()
        hPutStrLn h . show $ response
      modify (\s -> s{goingToSwitch = Just d})
      handleMessages h
    _ ->  do
      liftIO $  do
#if debug
        putStr "sending: "
        print response
#endif
        hPutStrLn h . show $ response
      handleMessages h


respond :: ServerMessage -> Bot ClientMessage
respond message = case message of
  Join -> do
#if gui
#else
    liftIO $ putStrLn "Joined"
#endif
    return PingMessage
  YourCar n -> do
#if gui
#else
    liftIO $ putStrLn "YourCar"
#endif
    updateBotName n
    return PingMessage
  GameInit gameInit@GameInitData{race = r} -> do
    updateWithRace r
#if gui
#else
    liftIO $ putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
#endif
    return PingMessage
  Lapfinished -> do
    a <- gets maxLapAngle
    maybeIncreaseForce a
    resetMaxLapAngle
    makeDecision
  CarPositions carPositions -> do
    botname <- gets ourName
    let mus = findCar botname carPositions
    case mus of
      Nothing -> do 
#if gui
#else
         liftIO $ putStrLn "WAT, we don't exist!?!"
#endif
         return (ThrottleMessage 0.5)
      (Just us) -> do
        updateWithPosition us
#if gui
        ui <- gets ui
        state <- get
        liftIO $ updateUI ui state
#else
#endif
        makeDecision
  GameStart -> do
    backToZero
#if gui
#else
    liftIO (putStrLn "REVVING ENGINES!!!!")
#endif
    modify (\s -> s{gameStarted = True})
    return PingMessage
  Spawn -> do
#if gui
#else
    liftIO (putStrLn "SPAWN")
#endif
    return PingMessage
  GameEnd -> do
    backToZero
#if gui
#else
    liftIO (putStrLn "Game ended!")
#endif
    return PingMessage
  Turbo d t f -> do
    modify (\s -> s{turboAvailable = True})
    return PingMessage
    --return Stop
  TournamentEnd -> do
#if gui
#else
    liftIO (putStrLn "Tournament ended!")
#endif
    return Stop
  Crash name -> do
    botname <- gets ourName
    when (name == botname) (do
      modify (\s -> s{turboAvailable = False})
      decreaseMaxForceMuch
      logMsg "We crashed!")
    return PingMessage
  Unknown msgType -> do
#if gui
#else
    liftIO $ putStrLn $ "Unknown message: " ++ msgType
#endif
    return PingMessage
