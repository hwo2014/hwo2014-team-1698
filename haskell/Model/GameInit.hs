{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module Model.GameInit where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))
import Data.Maybe
import Control.Monad

import Model.Direction

-- Dimension

data Dimension = Dimension {
  dimensionLength   :: Float,
  width             :: Float,
  guideFlagPosition :: Float
} deriving (Eq,Ord,Show)

instance FromJSON Dimension where
  parseJSON (Object v) =
    Dimension <$>
    (v .: "length") <*>
    (v .: "width") <*>
    (v .: "guideFlagPosition")

-- CarId

data CarId = CarId {
  color     :: String,
  carIdName :: String
} deriving (Eq,Ord,Show)

instance FromJSON CarId where
  parseJSON (Object v) =
    CarId <$>
    (v .: "color") <*>
    (v .: "name")

-- Car

data Car = Car {
  carId         :: CarId,
  dimensions :: Dimension
} deriving (Eq,Ord,Show)

instance FromJSON Car where
  parseJSON (Object v) =
    Car <$>
    (v .: "id") <*>
    (v .: "dimensions")

-- Lane

data Lane = Lane {
  distanceFromCenter :: Int,
  index              :: Int
} deriving (Eq,Ord,Show)

instance FromJSON Lane where
  parseJSON (Object v) =
    Lane <$>
    (v .: "distanceFromCenter") <*>
    (v .: "index")

-- Piece

isBend StraightPiece{} = False
isBend BendPiece{} = True

pieceDirection StraightPiece{} = Nothing
pieceDirection BendPiece{pieceAngle = a}
  | a >= 0 = Just DirRight
  | otherwise = Just DirLeft

pieceLength _ (StraightPiece l _) = l
pieceLength 
  Lane{distanceFromCenter = d}
  (BendPiece a r _) = 2*pi*(fromIntegral r - (fromIntegral d * signum a))* (abs a/360)

data Piece = 
 StraightPiece{
  straightLength :: Float,
  hasSwitch :: Bool}
 | BendPiece{
   pieceAngle :: Float,
   pieceRadius :: Int,
   hasSwitch :: Bool
 } deriving (Eq,Ord,Show)

unRaw :: RawPiece -> Piece
unRaw rp =
  if isJust (rawPieceLength rp)
  then StraightPiece (fromJust (rawPieceLength rp)) (fromMaybe False (rawSwitch rp))
  else BendPiece (fromMaybe 0.0 (rawPieceAngle rp)) (fromMaybe 0 (rawRadius rp)) (fromMaybe False (rawSwitch rp))

instance FromJSON Piece where
  parseJSON = liftM unRaw . parseJSON

 
data RawPiece = RawPiece {
  rawPieceLength :: Maybe Float,
  rawSwitch :: Maybe Bool,
  rawRadius :: Maybe Int,
  rawPieceAngle  :: Maybe Float,
  pieceBridge :: Maybe Bool
} deriving (Show)

instance FromJSON RawPiece where
  parseJSON (Object v) =
    RawPiece <$>
    (v .:? "length") <*>
    (v .:? "switch") <*>
    (v .:? "radius") <*>
    (v .:? "angle")  <*>
    (v .:? "bridge")

-- StartingPoint

data StartingPoint = StartingPoint {
  position :: Position,
  angle    :: Float
} deriving (Eq,Ord,Show)

instance FromJSON StartingPoint where
  parseJSON (Object v) =
    StartingPoint <$>
    (v .: "position") <*>
    (v .: "angle")

-- Position

data Position = Position {
  x :: Float,
  y :: Float
} deriving (Eq,Ord,Show)

instance FromJSON Position where
  parseJSON (Object v) =
    Position <$>
    (v .: "x") <*>
    (v .: "y")

-- Track

data Track = Track {
  name          :: String,
  startingPoint :: StartingPoint,
  pieces        :: [Piece],
  lanes         :: [Lane]
} deriving (Eq,Ord,Show)

instance FromJSON Track where
  parseJSON (Object v) =
    Track <$>
    (v .: "name") <*>
    (v .: "startingPoint") <*>
    (v .: "pieces") <*>
    (v .: "lanes")

-- RaceSession

data RaceSession = RaceSession {
  laps :: Maybe Int,
  durationMs :: Maybe Int,
  maxLapTimeMs :: Maybe Int,
  quickRace :: Maybe Bool
} deriving (Eq,Ord,Show)

instance FromJSON RaceSession where
  parseJSON (Object v) =
    RaceSession <$>
    (v .:? "laps") <*>
    (v .:? "durationMs") <*>
    (v .:? "maxLapTimeMs") <*>
    (v .:? "quickRace")

-- Race

data Race = Race {
  track :: Track,
  cars  :: [Car],
  raceSession :: RaceSession
} | EmptyRace deriving (Eq,Ord,Show)

instance FromJSON Race where
  parseJSON (Object v) =
    Race <$>
    (v .: "track") <*>
    (v .: "cars") <*>
    (v .: "raceSession")

-- GameInitData

data GameInitData = GameInitData {
  race :: Race
} deriving (Eq,Ord,Show)

instance FromJSON GameInitData where
  parseJSON (Object v) =
    GameInitData <$>
    (v .: "race")

-- Helpers

players :: GameInitData -> [CarId]
players gameInit =
  map (\car -> carId $ car) $ cars $ race gameInit

piecesOfGame :: GameInitData -> [Piece]
piecesOfGame gameInit =
  pieces $ track $ race gameInit

lanesOfGame :: GameInitData -> [Lane]
lanesOfGame gameInit =
  lanes $ track $ race gameInit

reportGameInit :: GameInitData -> String
reportGameInit gameInit =
  "Players: " ++ (show $ players gameInit) ++ ", Track: " ++ show (length $ piecesOfGame gameInit) ++ " pieces"
