module Model.Direction where

data Direction =
  DirLeft
  | DirRight
  deriving (Eq,Ord)

instance Show Direction where
  show DirLeft = "Left"
  show DirRight = "Right"
