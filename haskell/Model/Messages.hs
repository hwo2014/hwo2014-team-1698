{-# LANGUAGE OverloadedStrings #-}
module Model.Messages where

import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Data.Aeson.Types
import Control.Applicative
import Text.Printf

import Model.Direction
import Model.GameInit
--import Model.Spawn
import Model.CarPosition

data ServerMessage =
  Join 
  | GameInit GameInitData
  | CarPositions [CarPosition]
  | GameStart
  | GameEnd
  | YourCar String
  | TournamentEnd
  | Crash String
  | Lapfinished
  | Unknown String
  | Spawn
  | Turbo{
     turboDurationMilliseconds :: Float,
     turboDurationTicks :: Int,
     turboFactor :: Float
  }
   deriving (Eq,Ord,Show)

decodeMessage :: String -> Value -> Parser ServerMessage
decodeMessage msgType msgData
  | msgType == "join" = return Join
  | msgType == "gameInit" = GameInit <$> parseJSON msgData
  | msgType == "carPositions" = CarPositions <$> parseJSON msgData
  | msgType == "crash" = Crash <$> (parseJSON msgData >>= (.: "name"))
  | msgType == "yourCar" = YourCar <$> (parseJSON msgData >>= (.: "name"))
  | msgType == "gameStart" = return GameStart
  | msgType == "gameEnd" = return GameEnd
  | msgType == "spawn" = return Spawn
  | msgType == "tournamentEnd" = return TournamentEnd
  | msgType == "lapFinished"  = return Lapfinished
  | msgType == "turboAvailable" = (\(Object v) -> Turbo <$>
        v .: "turboDurationMilliseconds" <*>
        v .: "turboDurationMilliseconds" <*>
        v .: "turboDurationMilliseconds") msgData
  | otherwise = return $ Unknown (msgType ++ show msgData)

instance FromJSON ServerMessage where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    decodeMessage msgType msgData
  parseJSON x          = fail $ "Not an JSON object: " ++ show x

data ClientMessage =
  JoinMessage{
     botName :: String,
     botKey :: String
    }
  | CreateRaceMessage{
    botId :: ClientMessage,
    trackName :: String,
    password :: String,
    carCount :: Int
  }
  | ThrottleMessage{
     amount :: Float
    }
  | SwitchLane{
     dir :: Direction
    }
  | PingMessage
  | TurboMessage{ messageData :: String }
  | Stop
   deriving (Eq,Ord)

instance Show ClientMessage where
  show (SwitchLane dir) =
    "{\"msgType\":\"switchLane\",\"data\":\"" ++ show dir ++ "\"}"
  show (JoinMessage botname botkey) = 
    "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
  show (CreateRaceMessage (JoinMessage botName botKey) trackName password carCount) = 
    "{\"msgType\": \"createRace\", \"data\": { \"botId\": { \"name\": \""++botName++"\", \"key\": \""++botKey++"\" }" 
    ++", \"trackName\": \""++trackName++"\", \"password\": \""++password++"\", \"carCount\": "++(show carCount)++" }}"
  show (ThrottleMessage amount) =
    "{\"msgType\":\"throttle\",\"data\":" ++ (printf "%.2f" amount) ++ "}"
  show PingMessage = "{\"msgType\":\"ping\",\"data\":{}}"
  show (TurboMessage d) = "{\"msgType\":\"turbo\",\"data\":\""++d++"\"}"
  show Stop = "STOP"
