module Decision.MakeDecision where

import Control.Monad.State
import Data.List

import GameState
import Model.Messages
import Model.GameInit
import Model.Direction
import Decision.Physics.Force

isLongStraight = do
    ps <- gets nextPieces
    p <- gets currPiece
    di <- gets distInto
    let straights =  takeWhile (not . isBend) $ (p:ps)
    case p of
      BendPiece{} -> return False
      StraightPiece{} -> 
        return ( 500 < (sum . map straightLength $ straights) - di -- far enough for a boost
          || length straights == (length (p:ps))) -- just the finishline left

-- | Make a decision of what Client message to send
makeDecision :: Bot ClientMessage
makeDecision = do
  nextturnSpeed <- nextTurnSpeed
  currSpeed <- gets speed
  d <- gets (fromIntegral . distanceFromCenter . currLane)
  p <- gets currPiece
  nestP <- gets nextPieces
  ils <- isLongStraight
  ta <- gets turboAvailable
  if ta && ils 
    then do 
      modify(\s -> s{ turboAvailable = False })
      return (TurboMessage "gogogo")
    else do
      break <- shouldBrake nextturnSpeed currSpeed
      throttle <- chooseThrottle currSpeed
      modify(\s -> s{currentThrottle = throttle})
      sw <- shouldSwitchLane
      case (sw,break && not (isBend p)) of
        (Just p,_)  -> return p
        (_,True) -> logMsg "Braking!" >> return (ThrottleMessage 0.0)
        _        -> logMsg ("Throttle: " ++ show throttle) >> return (ThrottleMessage throttle)

-- | Should we be switching the lane now?
shouldSwitchLane = do
  npcs <- gets nextPieces
  let nb = takeWhile (not . hasSwitch) . drop 1 . dropWhile (not . hasSwitch) $ npcs
  l <- gets currLane
  rl <- gets laneToRight
  ll <- gets laneToLeft
  let currDist = sum . map (pieceLength l) $ nb
  let leftDist = fmap (\x -> sum . map (pieceLength x) $ nb) ll
  let rightDist = fmap (\x -> sum . map (pieceLength x) $ nb) rl
  isSwitch <- gets goingToSwitch
  canswitchLeft <- gets canSwitchLeft
  canswitchRight <- gets canSwitchRight
  if Just currDist > leftDist && isSwitch /= Just DirLeft && canswitchLeft
    then return (Just (SwitchLane DirLeft))
    else (if Just currDist > rightDist && isSwitch /= Just DirRight && canswitchRight
      then return (Just (SwitchLane DirRight))
      else return Nothing)

-- | Distance to the next non-turn piece
distToNextStraight :: Bot Float
distToNextStraight = do
  ps <- gets nextPieces
  l <- gets currLane
  let bendsLen = sum . map (pieceLength l) . takeWhile isBend $ ps
  p <- gets currPiece
  t <- gets distInto
  logMsg ("DistToNextStraight: " ++ show (bendsLen + (pieceLength l p - t)))
  return (bendsLen + (pieceLength l p - t))
             
-- | Distance to the next turn piece
distToNextTurn :: Bot Float
distToNextTurn = do
  ps <- gets nextPieces
  l <- gets currLane
  let straightsLen = sum . map (pieceLength l) . takeWhile (not . isBend) $ ps
  p <- gets currPiece
  t <- gets distInto
  return (straightsLen + (pieceLength l p - t))

-- How much of the current turn remaining, in angles.
angleLeft = do
  p <-  gets currPiece
  r <- realRadius p
  ps <- gets nextPieces
  let bends = takeWhile isBend (ps)
  d <- gets distInto
  l <- gets currLane 
  let disLeft = pieceLength l p - d
  logMsg ("realRadius: " ++ show r)
  logMsg ("disLeft: " ++ show disLeft)
  logMsg ("restOfAngles: " ++ show (sumAngles bends))
  let inRad = disLeft / r
  let anglLeftHere = inRad * 180 / pi
  logMsg ("angleLeft: " ++ show (sumAngles bends + anglLeftHere))
  return (sumAngles bends + anglLeftHere)
     


-- | If we should be Throttling up, how much?
chooseThrottle :: Float -> Bot Float
chooseThrottle currSpeed = do
  p <-  gets currPiece
  l <- gets currLane
  ps <- gets nextPieces
  if isBend p
    then do
      let bends = takeWhile isBend (p:ps)
      is <- getSegmentInSpeed bends
      ms <- getSegmentMaxSpeed bends
      dist <- distToNextStraight
      an <- angleLeft
      logMsg ("InSpeed: " ++ show is)
      if (an < 30) -- Is it time to accelerate out of the turn yet?
        then return (min 1.0 ((ms/10) + 0.4))
        else return (min 1.0 (is/10))
    else do
      dist <- distToNextTurn
      let ticks = dist / currSpeed
      if ticks <= 3
        then do
          let bends = takeWhile isBend (dropWhile (not . isBend) ps)
          is <- getSegmentInSpeed bends
          return (is/10)
        else 
          return 1.0

-- | The radius from the turn center
-- of the current piece to the lane we are in
realRadius StraightPiece{} = return 10000000
realRadius BendPiece{
  pieceAngle = a,
  pieceRadius = r'} = do
  d <- gets (distanceFromCenter . currLane)
  return ((fromIntegral r') - signum a * (fromIntegral d))

-- | Should we be braking?
shouldBrake nextSpeed currSpeed
  | currSpeed < nextSpeed = return False
  | otherwise = do
    let deceleration = 0.13 -- Negative acceleration if we set throttle
                            -- to zero now, likely to vary with the angle
                            -- of the current piece or the angle of the
                            -- car
    distNext <- distToNextTurn
    let ticksToNext = distNext / currSpeed
    return (deceleration * (ticksToNext - 2) <= currSpeed - nextSpeed)


-- | The In speed of the the next turn
nextTurnSpeed :: Bot Float
nextTurnSpeed = do
  ps <- liftM (takeWhile isBend . dropWhile (not . isBend)) $ gets nextPieces -- The next bends
  getSegmentInSpeed ps

sumAngles = sum . map getAngle
  where
    getAngle StraightPiece{} = 0
    getAngle BendPiece{pieceAngle = a} = abs a

-- | The In speed of the segment (The speed we should have as
-- we enter the segment
getSegmentInSpeed :: [Piece] -> Bot Float
getSegmentInSpeed ps = getSegmentMaxSpeed $ ps

-- | The maximum sustainable speed in the segment.
getSegmentMaxSpeed :: [Piece] -> Bot Float
getSegmentMaxSpeed [] = return 1000.0
getSegmentMaxSpeed ps = do
  l <- gets currLane
  f <- gets maxForce
  return (minimum . map (\x -> maxSpeed f x l) $ ps)

-- | The maximum sustainable speed of the given piece.
maxSpeed :: Float -> Piece -> Lane -> Float
maxSpeed _ StraightPiece{} _ = 1000.0 -- Actually infty
maxSpeed f 
  p@BendPiece{
   pieceAngle = a,
   pieceRadius = r'}
  Lane{distanceFromCenter = d} =
    let r = fromIntegral r' - signum a * fromIntegral d in
      centripetalSpeed r f 1
