module Decision.Physics.Force where
import Model.GameInit
import Decision.Physics.Types

-- | The centripetal force on a given piece with
-- the give
centripetalForce :: Float -> Speed -> Mass -> Force
centripetalForce r v m =
   m * v * v / r

-- | centripetalForce solved for speed
centripetalSpeed :: Float -> Force -> Mass -> Speed
centripetalSpeed r f m =
  sqrt (f * r / m)
