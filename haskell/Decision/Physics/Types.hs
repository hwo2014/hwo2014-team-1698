module Decision.Physics.Types where

type Force = Float
type Speed = Float
type Mass = Float
type Radius = Float
