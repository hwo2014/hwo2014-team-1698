{-# LANGUAGE OverloadedStrings #-}
module UI.Vty where
import Control.Concurrent (forkIO, threadDelay)
import GHC.IORef
import System.Exit
import Graphics.Vty.Widgets.All
import Graphics.Vty hiding (Button)
import qualified Data.Text as T
import GameState

setupUI = do
    s <- plainText "speed"
    a <- plainText "angle"
    l <- plainText "lane"
    n <- plainText "num laps"
    g <- plainText "going to switch"
    t <- plainText "traveled"
    tr <- plainText "throttle"
    lr <- textWidget wrap "lastRecieced"
    return $ View{
        uiSpeed = s,
        uiOurAngle = a,
        uiInLane = l,
        uiNumLaps = n,
        uiGoingToSwitch = g,
        uiTraveled = t,
        uiCurrentThrottle = tr,
        uiLastRecieved = lr
    }

drawUI view = do
    placeholder <- plainText ""
    mainBox <- (vBox (uiSpeed view) (uiOurAngle view)) <--> (vBox (uiInLane view) (uiNumLaps view)) <--> (vBox (uiGoingToSwitch view) (uiTraveled view))
        <--> (vBox (uiCurrentThrottle view) (uiLastRecieved view))
    c <- newCollection
    fg <- newFocusGroup
    _ <- addToFocusGroup fg mainBox
    mainBox `onKeyPressed` \_ key _ ->
        if key == KASCII 'q' 
        then exitSuccess 
        else return False
    _ <- addToCollection c mainBox fg
    runUi c defaultContext

insertNewLine :: Int -> String -> String
insertNewLine n s = if length s < n then s else (take n s) ++ "\n" ++ insertNewLine n (drop n s)


updateUI :: View -> GameState -> IO ()
updateUI view state = do
  forkIO $ do
    --threadDelay $ 1 * 1000 * 1000
    schedule $ setText (uiSpeed view) (T.pack $ "Speed: " ++ (show (speed state)))
    schedule $ setText (uiCurrentThrottle view) (T.pack $ "Throttle: " ++ (show (currentThrottle state)))
    schedule $ setText (uiOurAngle view) (T.pack $ "Angle: " ++ (show (ourAngle state)))
    schedule $ setText (uiInLane view) (T.pack $ "Lane: " ++ (show (inLane state)))
    schedule $ setText (uiNumLaps view) (T.pack $ "Num laps: " ++ (show (numLaps state)))
    schedule $ setText (uiGoingToSwitch view) (T.pack $ "Switching: " ++ (show (goingToSwitch state)))
    schedule $ setText (uiTraveled view) (T.pack $ "Approximate distance traveled: " ++ (show (approxDistTraveled state)))
    schedule $ setText (uiLastRecieved view) (T.pack $ insertNewLine 100 ("Last recieved message: " ++ (show (lastRecieved state))))
  return ()

forkDraw view = do
  forkIO $ do
    drawUI view
  return ()

